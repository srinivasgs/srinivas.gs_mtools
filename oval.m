% oval.m
% oval is a better version of round, which rounds to how many ever
% significant digits you want and returns a string, so you can directly use it in your plots
% 
% 
% created by Srinivas Gorur-Shandilya at 10:20 , 09 April 2014. Contact me at http://srinivas.gs/contact/
% 
% This work is licensed under the Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License. 
% To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-sa/4.0/.
function [r] = oval(a,s)
% fix for negative numbers
if ischar(s)
	% a is a fraction, and we should parse it appropriately
	denom = 1;
	while floor(a*denom) ~= ceil(a*denom)
		denom = denom + 1;
	end
	num = a*denom;
	r = strcat(mat2str(num),'/',mat2str(denom));
else
	if a <0
		flip=1;
	else
		flip = 0;
	end
	a= abs(a);
	powerbase = round(log10(a));
	if powerbase > s  
	else
	    s = s - powerbase;
	end
	% get as many significant digits as needed before 0
	    a = round(a*10^(s));
	    % get back to the original number
	    r = mat2str(a*10^(-s));

	if flip
		r = strcat('-',r);
	end
end